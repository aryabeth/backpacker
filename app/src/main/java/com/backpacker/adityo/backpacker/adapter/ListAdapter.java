package com.backpacker.adityo.backpacker.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.backpacker.adityo.backpacker.customclass.Destinasi;
import com.backpacker.adityo.backpacker.R;

import java.util.ArrayList;

/**
 * Created by adityo on 4/3/2015.
 */
public class ListAdapter extends ArrayAdapter<Destinasi> {

    public ListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public ListAdapter(Context context, int resource, ArrayList<Destinasi> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {

            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.item_feed, null);

        }

        Destinasi p = getItem(position);

        if (p != null) {

            ImageView contentImage = (ImageView) v.findViewById(R.id.contentImage);
            TextView loc = (TextView) v.findViewById(R.id.txtLoc);
            TextView username = (TextView)v.findViewById(R.id.txtUname);
            if(contentImage != null) {
                contentImage.setImageBitmap(p.getImg());
            }

            if(loc != null){
                loc.setText(p.getLoc());
            }

            if(username != null){
                username.setText(p.getUsername());
            }


        }

        return v;

    }
}