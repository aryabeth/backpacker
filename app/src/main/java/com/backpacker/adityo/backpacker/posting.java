package com.backpacker.adityo.backpacker;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;


public class posting extends Activity implements LocationListener{
    TextView textTitle,txtError;
    ImageView imagepost;
    EditText txtLoc,txtPrice;
    Button buttShare;
    Switch sLoc;

    Bitmap bitmap;
    String path = "";


    String username;
    String response = null;

    LocationManager lm;
    Location location;
    String provider;
    private static final long MIN_TIME = 400;
    private static final float MIN_DISTANCE = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posting);


        txtLoc = (EditText)findViewById(R.id.txtLoc);
        txtPrice = (EditText)findViewById(R.id.txtPrice);
        imagepost = (ImageView)findViewById(R.id.imagepost);
        txtError = (TextView)findViewById(R.id.txtError);
        buttShare = (Button)findViewById(R.id.buttShare);
        sLoc = (Switch)findViewById(R.id.switchLoc);

        SharedPreferences sp = getSharedPreferences(Login.MyPREFERENCES, Context.MODE_PRIVATE);
        username = sp.getString(Login.name,"username");


        String temp = null;
        temp = posting.this.getIntent().getStringExtra("uri");
        /*BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inSampleSize = 4;*/
        Uri uri;
        if(temp != null){
            uri = Uri.parse(temp);
            //bitmap = BitmapFactory.decodeFile(uri.getPath(),opt);
            path = uri.getPath();
        } else{
            temp = posting.this.getIntent().getStringExtra("uriGaleri");
            uri = Uri.parse(temp);
            //bitmap = BitmapFactory.decodeFile(getImagePath(uri),opt);
            path = getImagePath(uri);
        }

        imagepost.setImageURI(uri);

/*
        int bytes = bitmap.getByteCount();
        ByteBuffer buffer = ByteBuffer.allocate(bytes); //Create a new buffer
        bitmap.copyPixelsToBuffer(buffer); //Move the byte data to the buffer
        final byte[] byteArray = buffer.array();
*/

/*
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 50, stream);
        final byte[] byteArray = stream.toByteArray();
*/

        buttShare.setOnClickListener(new View.OnClickListener() {


            public void onClick(View v) {
                String loc = txtLoc.getText().toString();
                String price = txtPrice.getText().toString();
                //String strImg = Base64.encodeToString(byteArray,Base64.DEFAULT);

                    //    Toast.makeText(posting.this,"masuk",Toast.LENGTH_LONG).show();

                    postingTask post = new postingTask();
                    post.execute(new String[]{username,loc,price,path});
            }
        });

        sLoc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    txtLoc.setHint("Locating...");
                    txtLoc.setEnabled(false);
                    buttShare.setEnabled(false);

                   try {

                       lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
                       provider = lm.getBestProvider(new Criteria(),true);
                       location = lm.getLastKnownLocation(provider);

                       lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,MIN_TIME,MIN_DISTANCE,posting.this);

                       if (location != null) {
                           onLocationChanged(location);
                       }

                   }catch (Exception e){
                       e.printStackTrace();
                   }


                } else {

                    txtLoc.setHint("Location");
                    txtLoc.setEnabled(true);
                    buttShare.setEnabled(true);
                }
            }
        });

    }



    private class postingTask extends AsyncTask<String, Void, String> {
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(posting.this);
            pDialog.setMessage("UPLOADING...");
            pDialog.setIndeterminate(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
           /* ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
            postParameters.add(new BasicNameValuePair("username", params[0] ));
            postParameters.add(new BasicNameValuePair("loc", params[1] ));
            postParameters.add(new BasicNameValuePair("price", params[2] ));
            postParameters.add(new BasicNameValuePair("img", params[3] ));*/
            String res = null;
            try {
                response = CustomHttpClient.executeHttpMultipartPost("http://progmob.esy.es/posting.php", new String[]{params[0],params[1],params[2],params[3]},location.getLatitude(),location.getLongitude());
                res=response.toString();
                res= res.replaceAll("\\s+","");
            }
            catch (Exception e) {

            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            if(result.equals("1")){
                pDialog.dismiss();
                Toast.makeText(posting.this, "WES DISHARE CUK!!!!", Toast.LENGTH_LONG).show();
                finish();
            }
            else{
                pDialog.dismiss();
                txtError.setText(result);
            }
        }//close onPostExecute
    }

    public String getImagePath(Uri uri){
        Cursor cur = getContentResolver().query(uri,null,null,null,null);
        cur.moveToFirst();
        String doc_id = cur.getString(0);
        doc_id = doc_id.substring(doc_id.lastIndexOf(":")+1);
        cur.close();

        cur = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,null,MediaStore.Images.Media._ID+" = ? ",new String[]{doc_id},null);
        cur.moveToFirst();
        String path = cur.getString(cur.getColumnIndex(MediaStore.Images.Media.DATA));
        cur.close();

        return path;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_posting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onLocationChanged(Location location) {
        buttShare.setEnabled(true);
        posting.this.location = location;
        Geocoder gcd = new Geocoder(posting.this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            txtLoc.setText(addresses.get(0).getLocality()+", " +addresses.get(0).getSubAdminArea());
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Enabled new provider " + provider,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Disabled provider " + provider,Toast.LENGTH_SHORT).show();
    }

}
