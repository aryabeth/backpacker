package com.backpacker.adityo.backpacker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;


public class register extends Activity {


    SharedPreferences sharedpreferences;

    EditText regUname, regPass,regRepass;
    TextView txt_error;
    Button btnReg, btnIn;

    String response = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        regUname = (EditText)findViewById(R.id.regUname);
        regPass = (EditText)findViewById(R.id.regPass);
        regRepass = (EditText)findViewById(R.id.regRepass);
        btnReg = (Button)findViewById(R.id.btnReg);
        btnIn = (Button)findViewById(R.id.btnIn);
        txt_error = (TextView)findViewById(R.id.txt_error);

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uname = regUname.getText().toString();
                String pwd = regPass.getText().toString();
                String repwd = regRepass.getText().toString();

                InputMethodManager imm = (InputMethodManager)getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(regUname.getWindowToken(), 0);

                if(pwd.equals(repwd)) {
                    validateUserTask task = new validateUserTask();
                    task.execute(new String[]{uname, pwd});

                } else {
                    txt_error.setText("password bedo cuk!");
                    txt_error.setVisibility(View.VISIBLE);
                }
             }

        });

        btnIn  .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(register.this, Login.class);
                startActivity(i);

            }
        });

    }



    @Override
    protected void onResume() {
        sharedpreferences=getSharedPreferences(Login.MyPREFERENCES,
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains(Login.name))
        {
            if(sharedpreferences.contains(Login.pass)){
                Intent i = new Intent(this,Backpacker.class);
                startActivity(i);
                this.finish();
            }
        }
        super.onResume();
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class validateUserTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
            postParameters.add(new BasicNameValuePair("uname", params[0] ));
            postParameters.add(new BasicNameValuePair("pass", params[1] ));
            String res = null;
            try {
                response = CustomHttpClient.executeHttpPost("http://progmob.esy.es/register.php", postParameters);
                res=response.toString();
                res= res.replaceAll("\\s+","");
            }
            catch (Exception e) {
                txt_error.setText(e.toString());
            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            if(result.equals("1")){
                //navigate to Main Menu
                SharedPreferences.Editor editor = sharedpreferences.edit();
                String username = regUname.getText().toString();
                String passwd = regPass.getText().toString();
                editor.putString(Login.name, username);
                editor.putString(Login.pass, passwd);
                editor.commit();


                Intent i = new Intent(register.this, Backpacker.class);
                startActivity(i);
                finish();
            }
            else{
                txt_error.setText(result);
                txt_error.setVisibility(View.VISIBLE);
            }
        }//close onPostExecute
    }// close validateUserTask
}
